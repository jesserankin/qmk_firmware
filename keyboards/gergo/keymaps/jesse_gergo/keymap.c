#include QMK_KEYBOARD_H

enum custom_keycodes {
  ALT_TAB_HOLD = SAFE_RANGE,
	CMD_TAB_HOLD,
	ALT_GRV_HOLD
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
		case ALT_TAB_HOLD:
			if (record->event.pressed) {
        register_code(KC_RALT);
				tap_code(KC_TAB);
      } else {
        unregister_code(KC_RALT);
      }
      break;
		case ALT_GRV_HOLD:
			if (record->event.pressed) {
        register_code(KC_RALT);
				tap_code(KC_GRV);
      } else {
        unregister_code(KC_RALT);
      }
      break;
		case CMD_TAB_HOLD:
			if (record->event.pressed) {
        register_code(KC_RGUI);
				tap_code(KC_TAB);
      } else {
        unregister_code(KC_RGUI);
      }
      break;
	}
	return true;
}

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[0] = LAYOUT_gergo(
	  KC_EQUAL, KC_Q, KC_W, KC_E, KC_R, KC_T,                                                              KC_Y, KC_U, KC_I, KC_O, KC_P, KC_BSPC, 
	  KC_LPRN, KC_A, KC_S, KC_D, KC_F, KC_G, KC_QUOT,                                      KC_MINS, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_MINUS,
		KC_RPRN, KC_Z, KC_X, LALT_T(KC_C), LCTL_T(KC_V), LGUI_T(KC_B), KC_EQL, TG(1),     RGUI(KC_SPC), KC_UNDS, RGUI_T(KC_N), RCTL_T(KC_M), RALT_T(KC_COMM), KC_DOT, KC_SLSH, KC_QUOTE,
		                               KC_BSPC, LT(2, KC_BSPC), LT(1,KC_ESC), KC_TRNS,     KC_TRNS, LT(1,KC_TAB), RSFT_T(KC_SPC), KC_ENT),
	// WINDOWS
	//[1] = LAYOUT_gergo(KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, LCTL_T(KC_B), KC_TRNS, KC_TRNS, KC_LGUI, KC_TRNS, RCTL_T(KC_N), KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_L, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS),
  // SYMBOLS
	[1] = LAYOUT_gergo(
		KC_GRAVE,KC_RABK,KC_PIPE,KC_HASH,KC_LBRACKET,KC_RBRACKET,             							  				KC_7,KC_8,KC_9,KC_ASTR,KC_KP_PLUS,KC_TRNS, 
		KC_TRNS,KC_TILD,KC_AMPR,KC_PERC,KC_LCBR,KC_RCBR, KC_TRNS,																KC_TRNS, KC_4,KC_5,KC_6,KC_PERC,KC_KP_MINUS,KC_EQUAL, 
		KC_TRNS,KC_BSLASH,KC_EXLM,KC_AT,KC_CIRC,KC_DLR, KC_TRNS, KC_TRNS, 								KC_TRNS, KC_TRNS, KC_1,KC_2,KC_3,KC_TRANSPARENT,KC_TRANSPARENT,KC_COMMA, 
																			KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, 								KC_TRNS, KC_TRNS, KC_0, KC_TRNS),
	// NUMBERS
	//[2] = LAYOUT_gergo(KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_P7, KC_P8, KC_P9, KC_PAST, KC_PPLS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_P4, KC_P5, KC_P6, KC_PERC, KC_PMNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_P0, KC_P1, KC_P2, KC_P3, KC_PDOT, KC_PSLS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_COMM, KC_0, KC_COMM),
	// MOVEMENT
	[2] = LAYOUT_gergo(
		KC_TRNS, KC_TRNS, KC_MS_WH_LEFT, KC_MS_WH_DOWN, KC_MS_WH_UP, KC_MS_WH_RIGHT,       																	KC_MS_WH_LEFT,KC_MS_WH_DOWN,KC_MS_WH_UP,KC_MS_WH_RIGHT, KC_TRNS, KC_TRNS, 
		KC_TRNS, KC_MS_WH_UP, KC_MS_WH_DOWN, KC_BTN2, KC_BTN1, KC_BTN3, KC_TRNS, 									    	     KC_TRNS, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_TRNS, KC_TRNS, 
 		KC_TRNS, KC_TRNS, KC_TRNS, KC_K,    KC_J, KC_SPC, KC_TRNS, KC_TRNS,           KC_TRNS, KC_TRNS, KC_HOME, KC_END,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
																		   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,     	KC_TRNS, RGUI(KC_GRV), CMD_TAB_HOLD, RGUI(KC_GRV)),

	// MOUSE
	[3] = LAYOUT_gergo(
		KC_TRNS, KC_TRNS, KC_TRNS, KC_MS_WH_LEFT, KC_MS_WH_RIGHT, RGUI(KC_GRV),       																	KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS, KC_TRNS, KC_TRNS, 
		KC_TRNS, KC_TRNS, KC_TRNS, KC_MS_WH_UP, KC_MS_WH_DOWN, CMD_TAB_HOLD, KC_TRNS,									    	     KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,   KC_TRNS, KC_TRNS, KC_TRNS, 
 		KC_TRNS, KC_TRNS, KC_TRNS, KC_K,    KC_J, KC_SPC, KC_TRNS, KC_TRNS,           KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
																		   KC_BTN2, KC_BTN1, KC_BTN3, KC_TRNS,     	KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS)
};

/* 
enum combo_events {
  DF_MOUSE_ON,
  DF_MOUSE_OFF,
};

const uint16_t PROGMEM mouse_on_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM mouse_off_combo[] = {KC_MS_WH_UP, KC_MS_WH_DOWN, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [DF_MOUSE_ON] = COMBO_ACTION(mouse_on_combo),
  [DF_MOUSE_OFF] = COMBO_ACTION(mouse_off_combo),
};

void process_combo_event(uint8_t combo_index, bool pressed) {
  switch(combo_index) {
    case DF_MOUSE_ON:
      if (pressed) {
        layer_on(3);
      }
      break;
    case DF_MOUSE_OFF:
      if (pressed) {
        layer_off(3);
      }
      break;
  }
}

*/
#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "keymap_nordic.h"
#include "keymap_french.h"
#include "keymap_spanish.h"
#include "keymap_hungarian.h"
#include "keymap_swedish.h"
//#include "pointing_device.h"

#define KC_MAC_UNDO LGUI(KC_Z)
#define KC_MAC_CUT LGUI(KC_X)
#define KC_MAC_COPY LGUI(KC_C)
#define KC_MAC_PASTE LGUI(KC_V)
#define KC_PC_UNDO LCTL(KC_Z)
#define KC_PC_CUT LCTL(KC_X)
#define KC_PC_COPY LCTL(KC_C)
#define KC_PC_PASTE LCTL(KC_V)
#define NO_TH ALGR(KC_T)
#define NO_ETH ALGR(KC_D)

enum custom_keycodes {
  RGB_SLD = SAFE_RANGE, // can always be here
  TOGGLE_LAYER_COLOR,
  EPRM,
  ALT_TAB_HOLD,
	CMD_TAB_HOLD,
	ALT_GRV_HOLD
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  // MAC
  [0] = LAYOUT_ergodox(KC_GRAVE,KC_1,KC_2,KC_3,KC_4,KC_5,KC_LEFT,KC_EQUAL,KC_Q,KC_W,KC_E,KC_R,KC_T,KC_QUOTE,KC_LPRN,KC_A,KC_S,KC_D,KC_F,KC_G,KC_RPRN,KC_Z,KC_X,LALT_T(KC_C),LCTL_T(KC_V),RGUI_T(KC_B),KC_EQUAL,KC_LCTRL,KC_LALT,KC_LGUI,TG(1),KC_BSPACE,KC_AUDIO_VOL_DOWN,KC_AUDIO_VOL_UP,KC_AUDIO_MUTE,LT(3,KC_BSPACE),LT(2,KC_ESCAPE),KC_HOME,KC_RIGHT,KC_6,KC_7,KC_8,KC_9,KC_0,KC_TRANSPARENT,KC_KP_MINUS,KC_Y,KC_U,KC_I,KC_O,KC_P,KC_BSPACE,KC_H,KC_J,KC_K,KC_L,KC_SCOLON,KC_MINUS,KC_UNDS,RGUI_T(KC_N),RCTL_T(KC_M),RALT_T(KC_COMMA),KC_DOT,KC_SLASH,KC_QUOTE,KC_ENTER,KC_LEFT,KC_DOWN,KC_UP,KC_RIGHT,KC_F14,KC_F15,RGUI(KC_SPACE),KC_END,LT(2,KC_TAB),RSFT_T(KC_SPACE)),

  // LINUX/WINDOWS
  [1] = LAYOUT_ergodox(KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,LCTL_T(KC_B),KC_TRANSPARENT,KC_LGUI,KC_LALT,KC_LCTRL,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,LT(4,KC_BSPACE),KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,RCTL_T(KC_N),KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_RCTRL,KC_RALT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_LGUI,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT),

  // SYMBOLS/NUMBERS
  [2] = LAYOUT_ergodox(KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,
  KC_GRAVE,KC_RABK,KC_PIPE,KC_HASH,KC_LBRACKET,KC_RBRACKET,KC_TRANSPARENT,
  KC_TRANSPARENT,KC_TILD,KC_AMPR,KC_PERC,KC_LCBR,KC_RCBR,
  KC_TRANSPARENT, KC_BSLASH,KC_EXLM,KC_AT,KC_CIRC,KC_DLR,
  KC_TRANSPARENT,RESET,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,RGB_HUI,RGB_HUD,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_CIRC,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,
  KC_7,KC_8,KC_9,KC_ASTR,KC_KP_PLUS,KC_TRANSPARENT,
  KC_4,KC_5,KC_6,KC_PERC,KC_KP_MINUS,KC_EQUAL,KC_TRANSPARENT,
  KC_1,KC_2,KC_3,KC_TRANSPARENT,KC_TRANSPARENT,KC_COMMA,KC_TRANSPARENT,
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,RGB_VAI,RGB_VAD,RGB_TOG,KC_DLR,KC_TRANSPARENT,KC_0),

  // MOVEMENT MAC
  [3] = LAYOUT_ergodox(KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_MS_WH_LEFT,KC_MS_WH_DOWN,KC_MS_WH_UP,KC_MS_WH_RIGHT,KC_TRANSPARENT,KC_TRANSPARENT,KC_LEFT,KC_DOWN,KC_UP,KC_RIGHT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_HOME,KC_END,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,RGUI(KC_GRAVE),CMD_TAB_HOLD),

  // MOVEMENT LINUX
  [4] = LAYOUT_ergodox(KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_MS_WH_LEFT,KC_MS_WH_DOWN,KC_MS_WH_UP,KC_MS_WH_RIGHT,KC_TRANSPARENT,KC_TRANSPARENT,KC_LEFT,KC_DOWN,KC_UP,KC_RIGHT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_HOME,KC_END,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,ALT_GRV_HOLD,ALT_TAB_HOLD),

  // MOUSE 
  [5] = LAYOUT_ergodox(KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_MS_WH_LEFT,KC_MS_WH_RIGHT,ALT_GRV_HOLD,KC_TRANSPARENT,
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_MS_WH_UP,KC_MS_WH_DOWN,ALT_TAB_HOLD,
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_K,KC_J,KC_SPACE, KC_TRANSPARENT,
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_MS_BTN2,   
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,
  KC_MS_BTN1,KC_MS_BTN3,KC_TRANSPARENT,KC_TRANSPARENT,
  
  KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT,KC_TRANSPARENT),
};

extern bool g_suspend_state;
extern rgb_config_t rgb_matrix_config;
bool disable_layer_color = 0;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}

const uint8_t PROGMEM ledmap[][DRIVER_LED_TOTAL][3] = {
    [1] = { {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233}, {169,238,233} },

    [2] = { {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {14,222,242}, {14,222,242}, {14,222,242}, {169,120,255}, {180,255,233}, {255,220,201}, {255,220,201}, {255,220,201}, {180,255,233}, {180,255,233}, {233,218,217}, {233,218,217}, {233,218,217}, {12,225,241}, {169,120,255}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {243,222,234}, {243,222,234}, {169,120,255}, {32,176,255}, {32,176,255}, {85,203,158}, {85,203,158}, {85,203,158}, {32,176,255}, {10,225,255}, {134,255,213}, {134,255,213}, {10,225,255}, {32,176,255}, {10,225,255}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0} },

    [3] = { {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {32,176,255}, {32,176,255}, {32,176,255}, {32,176,255}, {0,0,0}, {85,203,158}, {85,203,158}, {85,203,158}, {85,203,158}, {0,0,0}, {10,225,255}, {10,225,255}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0} },

    [4] = { {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {32,176,255}, {32,176,255}, {32,176,255}, {32,176,255}, {0,0,0}, {85,203,158}, {85,203,158}, {85,203,158}, {85,203,158}, {0,0,0}, {10,225,255}, {10,225,255}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0} },

    [5] = { {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {63,225,237}, {32,176,255}, {85,203,158}, {85,203,158}, {63,225,237}, {63,225,237}, {32,176,255}, {85,203,158}, {85,203,158}, {63,225,237}, {63,225,237}, {134,255,213}, {134,255,213}, {134,255,213}, {63,225,237}, {63,225,237}, {85,203,158}, {63,225,237}, {63,225,237}, {63,225,237} },
};

void set_layer_color(int layer) {
  for (int i = 0; i < DRIVER_LED_TOTAL; i++) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap[layer][i][0]),
      .s = pgm_read_byte(&ledmap[layer][i][1]),
      .v = pgm_read_byte(&ledmap[layer][i][2]),
    };
    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color( i, 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        rgb_matrix_set_color( i, rgb.r, rgb.g, rgb.b );
    }
  }
}

void rgb_matrix_indicators_user(void) {
  if (g_suspend_state || disable_layer_color) { return; }
  switch (biton32(layer_state)) {
    case 1:
      set_layer_color(1);
      break;
    case 2:
      set_layer_color(2);
      break;
    case 3:
      set_layer_color(3);
      break;
    case 4:
      set_layer_color(4);
      break;
    case 5:
      set_layer_color(5);
      break;
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case EPRM:
      if (record->event.pressed) {
        eeconfig_init();
      }
      return false;
    case RGB_SLD:
      if (record->event.pressed) {
        rgblight_mode(1);
      }
      return false;
    case RGB_TOG:
      if (record->event.pressed) {
        if (rgb_matrix_config.val) {
          rgb_matrix_sethsv(rgb_matrix_config.hue, rgb_matrix_config.sat, 0);
        } else {
          rgb_matrix_sethsv(rgb_matrix_config.hue, rgb_matrix_config.sat, 255);
        }
      }
      return false;
    case TOGGLE_LAYER_COLOR:
      if (record->event.pressed) {
        disable_layer_color ^= 1;
      }
      return false;

    case ALT_TAB_HOLD:
			if (record->event.pressed) {
        register_code(KC_RALT);
				tap_code(KC_TAB);
      } else {
        unregister_code(KC_RALT);
      }
      break;
		case ALT_GRV_HOLD:
			if (record->event.pressed) {
        register_code(KC_RALT);
				tap_code(KC_GRV);
      } else {
        unregister_code(KC_RALT);
      }
      break;
		case CMD_TAB_HOLD:
			if (record->event.pressed) {
        register_code(KC_RGUI);
				tap_code(KC_TAB);
      } else {
        unregister_code(KC_RGUI);
      }
      break;

  }
  return true;
}


uint32_t layer_state_set_user(uint32_t state) {

  uint8_t layer = biton32(state);

  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();
  switch (layer) {
    case 1:
      ergodox_right_led_1_on();
      break;
    case 2:
      ergodox_right_led_2_on();
      break;
    case 3:
      ergodox_right_led_3_on();
      break;
    case 4:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      break;
    case 5:
      ergodox_right_led_1_on();
      ergodox_right_led_3_on();
      break;
    case 6:
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
    case 7:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
    default:
      break;
  }
  return state;
};

enum combo_events {
  DF_MOUSE_ON,
  DF_MOUSE_OFF,
};

const uint16_t PROGMEM mouse_on_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM mouse_off_combo[] = {KC_MS_WH_UP, KC_MS_WH_DOWN, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
  [DF_MOUSE_ON] = COMBO_ACTION(mouse_on_combo),
  [DF_MOUSE_OFF] = COMBO_ACTION(mouse_off_combo),
};

void process_combo_event(uint8_t combo_index, bool pressed) {
  switch(combo_index) {
    case DF_MOUSE_ON:
      if (pressed) {
        layer_on(5);
      }
      break;
    case DF_MOUSE_OFF:
      if (pressed) {
        layer_off(5);
      }
      break;
  }
}

/* 
void pointing_device_task(void){
    //gather info and put it in:
    //mouseReport.x = 127 max -127 min
    //mouseReport.y = 127 max -127 min
    //mouseReport.v = 127 max -127 min (scroll vertical)
    //mouseReport.h = 127 max -127 min (scroll horizontal)
    //mouseReport.buttons = 0x1F (decimal 31, binary 00011111) max (bitmask for mouse buttons 1-5, 1 is rightmost, 5 is leftmost) 0x00 min

    report_mouse_t mouseReport = pointing_device_get_report();
    
    if (mouseReport.x != 0) {
      ergodox_right_led_1_on();
    } else {
      ergodox_right_led_1_off();
    }

    if (mouseReport.y != 0) {
      ergodox_right_led_2_on();
    } else {
      ergodox_right_led_2_off();
    }

		if (mouseReport.buttons != 0) {
      register_code(KC_C);
      unregister_code(KC_C);
      ergodox_right_led_3_on();
		} else {
      ergodox_right_led_3_off();
    }

    //send the report
    pointing_device_send();
}
*/

